///<reference path="geom.ts"/>

class Gesture {

    points:Point[];
    finished:bool;
    
    constructor() {
        this.points = [];
        this.finished = false;
    }
    
    clear() {
        this.points = [];
        this.finished = false;
    }
    
    add(p:Point):void {
        this.points.push(p);
    }
    
    end():void {
        this.finished = true;
    }
    
    
    draw(ctx:CanvasRenderingContext2D):void {
        ctx.beginPath();
        for (var i:number = 1; i < this.points.length; i++) {
            ctx.moveTo(this.points[i-1].x, this.points[i-1].y);
            ctx.lineTo(this.points[i].x, this.points[i].y);
        }
        if (this.finished == true) {
            ctx.moveTo(this.points[this.points.length-1].x, this.points[this.points.length-1].y);
            ctx.lineTo(this.points[0].x, this.points[0].y);
        }
        ctx.closePath();
        ctx.stroke();
    }
}


class GestureIgor extends Gesture {

    getCenter():Point {
        var center:Point = {x: 0, y: 0};
        for (var i:number = 0; i < this.points.length; i++) {
            center.x += this.points[i].x;
            center.y += this.points[i].y;
        }
        center.x = center.x / this.points.length;
        center.y = center.y / this.points.length;
        return center;
    }

    circleError(c:Circle):number {
        var err:number = 0.0;
        for (var i:number = 0; i < this.points.length; i++) {
            err += Math.abs(dist(c.center, this.points[i]) - c.radius);
        }
        console.log("Circle Error", c, err);
        return err;
    }

    estimateCircle():Circle {
        var center:Point = this.getCenter();
        
        var radius:number = 0;
        for (var i:number = 0; i < this.points.length; i++) {
            radius += dist(center, this.points[i]);
        }
        radius = radius / this.points.length;
        return new Circle(center, radius);
    }
    
    squareError(s:Square):number {
        var l1 = new Line(s.v1, s.v2);
        var l2 = new Line(s.v2, s.v3);
        var l3 = new Line(s.v3, s.v4);
        var l4 = new Line(s.v4, s.v1);
        
        console.log(l1, l2, l3, l4);
        
        var err:number = 0.0;
        for (var i:number = 0; i < this.points.length; i++) {
            err += Math.min(line_dist(this.points[i], l1), Math.min(line_dist(this.points[i], l2), Math.min(line_dist(this.points[i], l3), line_dist(this.points[i], l4)) ) );
        }
        console.log("Square Error", s, err);        
        if (isNaN(err)) return 1000000;
        return err;
    }
    
    closestTo(p:Point):Point {
        var d:number = 10000000, dst:number;
        var r:Point;
        for (var i:number = 0; i < this.points.length; i++) {
            dst = dist(this.points[i], p);
            if (dst < d) {
                d = dst;
                r = this.points[i];
            }
        }
        return r;
    }
    
    estimateSquare():Square {
        var top:Point = {x: 0, y: 100000};
        var bottom:Point = {x: 0, y: 0};
        var left:Point = {x: 10000, y: 0};
        var right:Point = {x: 0, y: 0};
        
        for (var i:number = 0; i < this.points.length; i++) {
            if (this.points[i].x < left.x) {
                left = this.points[i];
            }
            if (this.points[i].y < top.y) {
                top = this.points[i];
            }
            if (this.points[i].x > right.x) {
                right = this.points[i];
            }
            if (this.points[i].y > bottom.y) {
                bottom = this.points[i];
            }
        }
        
        var top_left:Point = this.closestTo({x: left.x, y: top.y});
        var bottom_left:Point =  this.closestTo({x: left.x, y: bottom.y});
        var top_right:Point =  this.closestTo({x: right.x, y: top.y});
        var bottom_right:Point =  this.closestTo({x: right.x, y: bottom.y});
        
        
        var sq1:Square = new Square(top, left, bottom, right);
        var sq2:Square = new Square(top_left, bottom_left, bottom_right, top_right);
        
        if (this.squareError(sq1) < this.squareError(sq2)) {
            return sq1;
        } else {
            return sq2;
        }
    }
    
    triangleError(t:Triangle):number {
        var l1 = new Line(t.v1, t.v2);
        var l2 = new Line(t.v2, t.v3);
        var l3 = new Line(t.v3, t.v1);
        
        var err:number = 0.0;
        for (var i:number = 0; i < this.points.length; i++) {
            err += Math.min(line_dist(this.points[i], l1), Math.min(line_dist(this.points[i], l2), line_dist(this.points[i], l3) ) );
        }
        
        if (isNaN(err)) return 1000000;
        console.log("Triangle Error", t, err);
        return err;
    }   
    
    estimateTriangle():Triangle {
        var top:Point = {x: 0, y: 100000};
        var bottom:Point = {x: 0, y: 0};
        var left:Point = {x: 10000, y: 0};
        var right:Point = {x: 0, y: 0};
        
        for (var i:number = 0; i < this.points.length; i++) {
            if (this.points[i].x < left.x) {
                left = this.points[i];
            }
            if (this.points[i].y < top.y) {
                top = this.points[i];
            }
            if (this.points[i].x > right.x) {
                right = this.points[i];
            }
            if (this.points[i].y > bottom.y) {
                bottom = this.points[i];
            }
        }
        var t:Triangle[] = [
            new Triangle(top, bottom, left),
            new Triangle(top, bottom, right),
            new Triangle(left, right, top),
            new Triangle(left, right, bottom)
        ];
        var err:number[] = [
            this.triangleError(t[0]),
            this.triangleError(t[1]),
            this.triangleError(t[2]),
            this.triangleError(t[3]),
        ];
        var m = Math.min(err[0], Math.min(err[1], Math.min(err[2], err[3])));
        for(var i:number = 0; i < 4; i++) {
            if (m == err[i]) return t[i];
        } 
        return t[3];
    }



    identifyShape():Drawable {
        var c:Circle = this.estimateCircle();
        var t:Triangle = this.estimateTriangle();
        var s:Square = this.estimateSquare();
        
        var c_err = this.circleError(c);
        var t_err = this.triangleError(t);
        var s_err = this.squareError(s);
        
        if (c_err <= t_err && c_err <= 2*s_err) {
            return c;
        } else if (t_err <= c_err && t_err <= 2*s_err) {
            return t;
        } else if (2*s_err <= t_err && 2*s_err <= c_err) {
            return s;
        }
    }
}


class Symbol {
    gestures:Gesture[];
    
    constructor() {
        this.gestures = [];
    }
    
    
}
