interface Point {
    x:number;
    y:number;
}

class Line {
    a:number;
    b:number;
    c:number;
    
    constructor(p0:Point, p1:Point) {
        this.a = - (p1.y - p0.y)/(p1.x - p0.x);
        this.b = 1;
        this.c = - (p1.x*p0.y - p0.x*p1.y)/(p1.x - p0.x);
        if (p0.x == p1.x) {
            this.b = p0.x;
        }
    }
}

function dist(a:Point, b:Point):number {
    return Math.sqrt( Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) );
}

function line_dist(p:Point, l:Line):number {
    if (isNaN(l.a) || l.a == Infinity || l.a == -Infinity) {
        return Math.abs(l.b - p.x);
    } else {
        return Math.abs(l.a*p.x + l.b*p.y + l.c)/Math.sqrt(l.a*l.a + l.b*l.b);
    }
}


interface Drawable {
    draw: (ctx: CanvasRenderingContext2D) => void;
}

class Circle {

    constructor(public center:Point, public radius:number) { }
    
    
    draw(ctx:CanvasRenderingContext2D):void {
        ctx.beginPath();
        ctx.arc(this.center.x, this.center.y, this.radius, 0, Math.PI*2, false);
        ctx.closePath();
        ctx.stroke();
    }
}

class Triangle {

    constructor(public v1:Point, public v2:Point, public v3:Point) { }
    
    draw(ctx:CanvasRenderingContext2D):void {
        ctx.beginPath();
    
        ctx.moveTo(this.v1.x, this.v1.y);
        ctx.lineTo(this.v2.x, this.v2.y);
        
        ctx.moveTo(this.v2.x, this.v2.y);
        ctx.lineTo(this.v3.x, this.v3.y);
        
        ctx.moveTo(this.v3.x, this.v3.y);
        ctx.lineTo(this.v1.x, this.v1.y);
        
        ctx.closePath();
        ctx.stroke();
    }
}

class Square {
    
    constructor(public v1:Point, public v2:Point, public v3:Point, public v4:Point) { }
    
    draw(ctx:CanvasRenderingContext2D):void {
        ctx.beginPath();
        
        ctx.moveTo(this.v1.x, this.v1.y);
        ctx.lineTo(this.v2.x, this.v2.y);
        
        ctx.moveTo(this.v2.x, this.v2.y);
        ctx.lineTo(this.v3.x, this.v3.y);
        
        ctx.moveTo(this.v3.x, this.v3.y);
        ctx.lineTo(this.v4.x, this.v4.y);
        
        ctx.moveTo(this.v4.x, this.v4.y);
        ctx.lineTo(this.v1.x, this.v1.y);
        
        ctx.closePath();
        ctx.stroke();
    }
}

