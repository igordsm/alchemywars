///<reference path="geom.ts"/>
///<reference path="gestures.ts"/>
///<reference path="touch.ts"/>



var g = new GestureIgor();
var ctx:CanvasRenderingContext2D;
var cv:HTMLCanvasElement;

function main() {
    cv = <HTMLCanvasElement> document.getElementById("cv");
    setup_mouse_events();
    setup_touch_events();
    cv.width = window.innerWidth;
    cv.height = window.innerHeight;
    ctx = cv.getContext("2d");
}

function setup_mouse_events() {
    var mouse_listener = function(evt:MouseEvent) {
            mouse_move(evt.pageX, evt.pageY);
            evt.preventDefault();
        };
    cv.addEventListener('mousedown', function(evt:MouseEvent) {
        poly_start();
        cv.addEventListener('mousemove', mouse_listener);
    });
    cv.addEventListener('mouseup', function(evt:MouseEvent) {
        poly_end();
        cv.removeEventListener('mousemove', mouse_listener);
    });
}

function setup_touch_events() {
    var touch_listener = function(evt:TouchEvent) {
            mouse_move(evt.touches[0].pageX, evt.touches[0].pageY);
            evt.preventDefault();
        };
    cv.addEventListener('touchstart', function(evt:TouchEvent) {
        poly_start();
        cv.addEventListener('touchmove', touch_listener);
    });
    cv.addEventListener('touchend', function(evt:TouchEvent) {
        poly_end();
        cv.removeEventListener('touchmove', touch_listener);
    });
}

function poly_start() {
    g.clear();
}

function poly_end() {
    g.finished = true;
    g.draw(ctx);
    
    var d:Drawable = g.identifyShape();
    ctx.strokeStyle = "#FF0000";
    d.draw(ctx);
}


function mouse_move(x:number, y:number) {
    g.add({x: x, y: y});
    ctx.strokeStyle = "#000000";
    g.draw(ctx);
}
