var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SCREEN_WIDTH = 480;
var SCREEN_HEIGHT = 320;
var RYU_WIDTH = 50;
var RYU_HEIGHT = 90;
var IDLE_RIGHT = 0;
var IDLE_LEFT = 1;
var WALKING_RIGHT = 2;
var WALKING_LEFT = 3;
var IDLE_SIZE = 4;
var WALKING_SIZE = 5;
var Monster = (function () {
    function Monster(x, y, imagePath) {
        var _this = this;
        this.image = null;
        this.x = x;
        this.y = y;
        this.image = new Image();
        this.image.src = imagePath;
        this.imageReady = false;
        this.image.onload = function () {
            _this.imageReady = true;
        };
        this.setAnimation(IDLE_RIGHT, 1, true);
    }
    Monster.prototype.setAnimation = function (animation, size, goingRight) {
        this.animation = animation;
        this.animationSize = size;
        this.goingRight = goingRight;
        if(this.goingRight) {
            this.animationFrame = 0;
        } else {
            this.animationFrame = this.animationSize - 1;
        }
    };
    Monster.prototype.increaseAnimationFrame = function (step) {
        if(this.goingRight) {
            this.animationFrame += step;
            while(this.animationFrame >= this.animationSize) {
                this.animationFrame -= this.animationSize;
            }
        } else {
            this.animationFrame -= step;
            while(this.animationFrame < 0) {
                this.animationFrame += this.animationSize;
            }
        }
    };
    Monster.prototype.draw = function (context) {
        if(this.imageReady) {
            context.drawImage(this.image, this.animationFrame * this.image.width, this.animation * this.image.height, this.image.width, this.image.height, this.x, this.y, this.image.width, this.image.height);
        }
    };
    return Monster;
})();
var Ryu = (function (_super) {
    __extends(Ryu, _super);
    function Ryu(x, y) {
        _super.call(this, x, y, "images/ryu_spritesheet.png");
        this.speed = 7;
        this.image.width = RYU_WIDTH;
        this.image.height = RYU_HEIGHT;
        this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
    }
    Ryu.prototype.move = function () {
        if(this.goingRight) {
            this.x += this.speed;
        } else {
            this.x -= this.speed;
        }
        this.bounce();
    };
    Ryu.prototype.bounce = function () {
        if(this.x < 0) {
            this.x = 0;
            this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        } else if(this.x > SCREEN_WIDTH - RYU_WIDTH) {
            this.x = SCREEN_WIDTH - RYU_WIDTH;
            this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
        }
    };
    Ryu.prototype.update = function () {
        this.move();
        this.increaseAnimationFrame(1);
    };
    return Ryu;
})(Monster);
var Board = (function () {
    function Board(width, height) {
        var _this = this;
        this.background = null;
        this.backgroundReady = false;
        this.ryu = null;
        this.background = new Image();
        this.background.src = "images/background.png";
        this.background.width = width;
        this.background.height = height;
        this.background.onload = function () {
            _this.backgroundReady = true;
        };
        this.ryu = new Ryu(SCREEN_WIDTH - (RYU_WIDTH / 2), height / 2);
    }
    Board.prototype.isReady = function () {
        return this.backgroundReady;
    };
    Board.prototype.draw = function (context) {
        if(this.backgroundReady) {
            context.drawImage(this.background, 0, 0, this.background.width, this.background.height);
        }
        this.ryu.draw(context);
    };
    Board.prototype.update = function (context) {
        this.ryu.update();
        this.draw(context);
    };
    return Board;
})();
function main() {
    var canvas = document.getElementById("gameCanvas");
    canvas.width = SCREEN_WIDTH;
    canvas.height = SCREEN_HEIGHT;
    var context = canvas.getContext("2d");
    var board = new Board(canvas.width, canvas.height);
    setInterval(function () {
        board.update(context);
    }, 100);
}
main();
