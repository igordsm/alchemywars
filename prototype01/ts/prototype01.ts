// Pseudo defines for animations.
var SCREEN_WIDTH = 480;
var SCREEN_HEIGHT = 320;

var RYU_WIDTH = 50;
var RYU_HEIGHT = 90;

var IDLE_RIGHT = 0;
var IDLE_LEFT = 1;
var WALKING_RIGHT = 2;
var WALKING_LEFT = 3;

var IDLE_SIZE = 4;
var WALKING_SIZE = 5;

class Monster {
    // x, y and goingRight should be 'protected' instead of
    // 'public', but typescript does not have this feature. =(
    public x: number;
    public y: number;
    public goingRight: bool; // True when monster is looking to the right.

    // The image should be 'protected' instead of 'public',
    // but typescript does not support that.
    public image: HTMLImageElement = null; 
    private imageReady: bool;

    private animation: number;      // Row index in the sprite sheet.
    private animationFrame: number; // Column index in the sprite sheet.
    private animationSize: number;  // Number of frames of current animation.

    constructor(x: number, y: number, imagePath: string) {
        this.x = x;
        this.y = y;
        this.image = new Image();
        this.image.src = imagePath;
        this.imageReady = false;
        this.image.onload = () => {
            this.imageReady = true;
        }
        this.setAnimation(IDLE_RIGHT, 1, true);
    }

    setAnimation(animation: number, size: number, goingRight: bool) {
        this.animation = animation;
        this.animationSize = size;
        this.goingRight = goingRight;

        // POG to use mirrored animation row instead of
        // individualy mirrored frames in sprite sheet.
        if (this.goingRight) {
            this.animationFrame = 0;
        } else {
            this.animationFrame = this.animationSize - 1
        }
    }

    increaseAnimationFrame(step: number) {
        if (this.goingRight) {
            this.animationFrame += step;
            while (this.animationFrame >= this.animationSize) {
                this.animationFrame -= this.animationSize;
            }
        } else {
            this.animationFrame -= step;
            while (this.animationFrame < 0) {
                this.animationFrame += this.animationSize;
            }
        }
    }

    draw(context) {
        if (this.imageReady) {
            context.drawImage(this.image,
                              // x coordinate where to start clipping.
                              this.animationFrame * this.image.width,
                              // Y coordinate where to start clipping.
                              this.animation * this.image.height,
                              // Clipped image's dimensions.
                              this.image.width, this.image.height,
                              // Where to place clipped image inside canvas.
                              this.x, this.y,
                              // Dimensions of clipped image inside canvas.
                              this.image.width, this.image.height);
        }
    }
}

class Ryu extends Monster {
    private speed: number = 7; // Pixels/update.

    constructor(x: number, y: number) {
        super(x, y, "images/ryu_spritesheet.png");

        // Set the dimensions of the frame / clipped image from sprite sheet.
        this.image.width = RYU_WIDTH;
        this.image.height = RYU_HEIGHT;

        //this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
    }

    // TODO: Use a dt to move proportionally to time.
    private move() {
        if (this.goingRight) {
            this.x += this.speed;
        } else {
            this.x -= this.speed;
        }
        this.bounce();
    }

    private bounce() {
        if (this.x < 0) {
            this.x = 0;
            this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        } else if (this.x > SCREEN_WIDTH - RYU_WIDTH) {
            this.x = SCREEN_WIDTH - RYU_WIDTH;
            this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
        }
    }

    update() {
        this.move();
        this.increaseAnimationFrame(1);
    }
}

class Board {
    private background: HTMLImageElement = null;
    private backgroundReady: bool = false;
    private ryu: Ryu = null;

    constructor(width: number, height: number) {
        this.background = new Image();
        this.background.src = "images/background.png";
        this.background.width = width;
        this.background.height = height;
        this.background.onload = () => {
            this.backgroundReady = true;
            //console.log("Background ready");
        }

        this.ryu = new Ryu(SCREEN_WIDTH - (RYU_WIDTH / 2), height / 2);
    }

    isReady(): bool {
        return this.backgroundReady;
    }

    // TODO: I don't know which type 'context' should be.
    draw(context) {
        if (this.backgroundReady) {
            // TODO: Implement double buffer.
            context.drawImage(this.background, 0, 0,
                              this.background.width,this.background.height);
        }
        this.ryu.draw(context);
    }

    update(context) {
        this.ryu.update();
        this.draw(context);
    }
}

function main() {
    var canvas = <HTMLCanvasElement>document.getElementById("gameCanvas");
    canvas.width = SCREEN_WIDTH;
    canvas.height = SCREEN_HEIGHT;

    var context = canvas.getContext("2d");
    var board = new Board(canvas.width, canvas.height);

    setInterval(function () {board.update(context)}, 100);
}

main();
