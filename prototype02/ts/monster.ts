var IDLE_RIGHT = 0;
var IDLE_LEFT = 1;
var WALKING_RIGHT = 2;
var WALKING_LEFT = 3;

var IDLE_SIZE = 4;
var WALKING_SIZE = 5;

class Monster {
    // x, y and goingRight should be 'protected' instead of
    // 'public', but typescript does not have this feature. =(
    public x: number;
    public y: number;
    public goingRight: bool; // True when monster is looking to the right.

    // The image should be 'protected' instead of 'public',
    // but typescript does not support that.
    public image: HTMLImageElement = null; 
    private imageReady: bool;

    private animation: number;      // Row index in the sprite sheet.
    private animationFrame: number; // Column index in the sprite sheet.
    private animationSize: number;  // Number of frames of current animation.

    constructor(x: number, y: number, imagePath: string) {
        this.x = x;
        this.y = y;
        this.image = new Image();
        this.image.src = imagePath;
        this.imageReady = false;
        this.image.onload = () => {
            this.imageReady = true;
            //console.log("Monster image ready.");
        }
        this.setAnimation(IDLE_RIGHT, 1, true);
    }

    setAnimation(animation: number, size: number, goingRight: bool) {
        this.animation = animation;
        this.animationSize = size;
        this.goingRight = goingRight;

        // POG to use mirrored animation row instead of
        // individualy mirrored frames in sprite sheet.
        if (this.goingRight) {
            this.animationFrame = 0;
        } else {
            this.animationFrame = this.animationSize - 1
        }
    }

    increaseAnimationFrame(step: number) {
        if (this.goingRight) {
            this.animationFrame += step;
            while (this.animationFrame >= this.animationSize) {
                this.animationFrame -= this.animationSize;
            }
        } else {
            this.animationFrame -= step;
            while (this.animationFrame < 0) {
                this.animationFrame += this.animationSize;
            }
        }
    }

    isReadyToDraw(): bool {
        return this.imageReady;
    }

    draw(context) {
        if (this.imageReady) {
            context.drawImage(this.image,
                              // x coordinate where to start clipping.
                              this.animationFrame * this.image.width,
                              // Y coordinate where to start clipping.
                              this.animation * this.image.height,
                              // Clipped image's dimensions.
                              this.image.width, this.image.height,
                              // Where to place clipped image inside canvas.
                              this.x, this.y,
                              // Dimensions of clipped image inside canvas.
                              this.image.width, this.image.height);
        }
    }
}
