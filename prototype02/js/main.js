var List = (function () {
    function List() {
        this.firstItem = undefined;
        this.lastItem = undefined;
        this.listSize = 0;
    }
    List.prototype.newListItem = function (object) {
        return {
            item: object,
            next: undefined,
            previous: undefined
        };
    };
    List.prototype.first = function () {
        return this.firstItem;
    };
    List.prototype.last = function () {
        return this.lastItem;
    };
    List.prototype.size = function () {
        return this.listSize;
    };
    List.prototype.isEmpty = function () {
        return (this.listSize == 0);
    };
    List.prototype.itemAt = function (position) {
        if(position < 0 || position >= this.listSize) {
            return undefined;
        }
        var item = this.firstItem;
        for(var i = 0; i < position; i++) {
            item = item.next;
        }
        return item;
    };
    List.prototype.push = function (object) {
        if(this.listSize == 0) {
            var newItem = this.newListItem(object);
            this.firstItem = newItem;
            this.lastItem = newItem;
            this.listSize++;
            return true;
        }
        return this.addAfter(object, this.lastItem);
    };
    List.prototype.pop = function () {
        if(this.listSize = 0) {
            return undefined;
        }
        var item = this.lastItem;
        this.remove(this.lastItem);
    };
    List.prototype.addBefore = function (object, target) {
        if(target == null || target == undefined) {
            return false;
        }
        var newItem = this.newListItem(object);
        newItem.next = target;
        newItem.previous = target.previous;
        target.previous = newItem;
        if(target == this.firstItem) {
            this.firstItem = newItem;
        } else {
            newItem.previous.next = newItem;
        }
        this.listSize++;
        return true;
    };
    List.prototype.addAfter = function (object, target) {
        if(target == null || target == undefined) {
            return false;
        }
        var newItem = this.newListItem(object);
        newItem.next = target.next;
        newItem.previous = target;
        target.next = newItem;
        if(target == this.lastItem) {
            this.lastItem = newItem;
        } else {
            newItem.next.previous = newItem;
        }
        this.listSize++;
        return true;
    };
    List.prototype.remove = function (target) {
        if(target == null || target == undefined || this.isEmpty()) {
            return false;
        }
        if(this.listSize == 1) {
            this.firstItem = undefined;
            this.lastItem = undefined;
        } else if(target == this.firstItem) {
            target.next.previous = undefined;
            this.firstItem = target.next;
        } else if(target == this.lastItem) {
            target.previous.next = undefined;
            this.lastItem = target.previous;
        } else {
            target.previous.next = target.next;
            target.next.previous = target.previous;
        }
        this.listSize--;
        return true;
    };
    return List;
})();
var IDLE_RIGHT = 0;
var IDLE_LEFT = 1;
var WALKING_RIGHT = 2;
var WALKING_LEFT = 3;
var IDLE_SIZE = 4;
var WALKING_SIZE = 5;
var Monster = (function () {
    function Monster(x, y, imagePath) {
        var _this = this;
        this.image = null;
        this.x = x;
        this.y = y;
        this.image = new Image();
        this.image.src = imagePath;
        this.imageReady = false;
        this.image.onload = function () {
            _this.imageReady = true;
        };
        this.setAnimation(IDLE_RIGHT, 1, true);
    }
    Monster.prototype.setAnimation = function (animation, size, goingRight) {
        this.animation = animation;
        this.animationSize = size;
        this.goingRight = goingRight;
        if(this.goingRight) {
            this.animationFrame = 0;
        } else {
            this.animationFrame = this.animationSize - 1;
        }
    };
    Monster.prototype.increaseAnimationFrame = function (step) {
        if(this.goingRight) {
            this.animationFrame += step;
            while(this.animationFrame >= this.animationSize) {
                this.animationFrame -= this.animationSize;
            }
        } else {
            this.animationFrame -= step;
            while(this.animationFrame < 0) {
                this.animationFrame += this.animationSize;
            }
        }
    };
    Monster.prototype.isReadyToDraw = function () {
        return this.imageReady;
    };
    Monster.prototype.draw = function (context) {
        if(this.imageReady) {
            context.drawImage(this.image, this.animationFrame * this.image.width, this.animation * this.image.height, this.image.width, this.image.height, this.x, this.y, this.image.width, this.image.height);
        }
    };
    return Monster;
})();
var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RYU_WIDTH = 50;
var RYU_HEIGHT = 90;
var Ryu = (function (_super) {
    __extends(Ryu, _super);
    function Ryu(x, y) {
        _super.call(this, x, y, "images/ryu_spritesheet.png");
        this.speed = 7;
        this.image.width = RYU_WIDTH;
        this.image.height = RYU_HEIGHT;
        this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
    }
    Ryu.prototype.move = function () {
        if(this.goingRight) {
            this.x += this.speed;
        } else {
            this.x -= this.speed;
        }
        this.bounce();
    };
    Ryu.prototype.bounce = function () {
        if(this.x < 0) {
            this.x = 0;
            this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        } else if(this.x > SCREEN_WIDTH - RYU_WIDTH) {
            this.x = SCREEN_WIDTH - RYU_WIDTH;
            this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
        }
    };
    Ryu.prototype.update = function () {
        this.move();
        this.increaseAnimationFrame(1);
    };
    return Ryu;
})(Monster);
var Board = (function () {
    function Board(width, height) {
        var _this = this;
        this.background = null;
        this.backgroundReady = false;
        this.enemies = null;
        this.loading = true;
        this.background = new Image();
        this.background.src = "images/background.png";
        this.background.width = width;
        this.background.height = height;
        this.background.onload = function () {
            _this.backgroundReady = true;
        };
        this.enemies = new List();
        for(var i = 0; i < 3; i++) {
            var newEnemy = new Ryu(30 + 60 * i, 100);
            this.enemies.push(newEnemy);
        }
    }
    Board.prototype.isReady = function () {
        return this.backgroundReady;
    };
    Board.prototype.isLoading = function () {
        if(this.loading) {
            this.loading = !this.areImagesLoaded();
        }
        return this.loading;
    };
    Board.prototype.areImagesLoaded = function () {
        if(!this.backgroundReady) {
            return false;
        }
        for(var enemy = this.enemies.first(); enemy != undefined; enemy = enemy.next) {
            if(!enemy.item.isReadyToDraw()) {
                return false;
            }
        }
        return true;
    };
    Board.prototype.draw = function (context) {
        if(this.isLoading()) {
            context.font = "30px Arial";
            context.fillText("Loading", 32, 32);
            return;
        }
        context.drawImage(this.background, 0, 0, this.background.width, this.background.height);
        for(var enemy = this.enemies.first(); enemy != undefined; enemy = enemy.next) {
            enemy.item.draw(context);
        }
    };
    Board.prototype.update = function (context) {
        for(var enemy = this.enemies.first(); enemy != undefined; enemy = enemy.next) {
            enemy.item.update();
        }
        this.draw(context);
    };
    return Board;
})();
var SCREEN_WIDTH = 480;
var SCREEN_HEIGHT = 320;
function main() {
    var canvas = document.getElementById("gameCanvas");
    canvas.width = SCREEN_WIDTH;
    canvas.height = SCREEN_HEIGHT;
    var context = canvas.getContext("2d");
    var board = new Board(canvas.width, canvas.height);
    setInterval(function () {
        board.update(context);
    }, 100);
}
main();
