///<reference path="monster.ts" />

var RYU_WIDTH = 50;
var RYU_HEIGHT = 90;

class Ryu extends Monster {
    private speed: number = 7; // Pixels/update.

    constructor(x: number, y: number) {
        super(x, y, "images/ryu_spritesheet.png");

        // Set the dimensions of the frame / clipped image from sprite sheet.
        this.image.width = RYU_WIDTH;
        this.image.height = RYU_HEIGHT;

        //this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
    }

    // TODO: Use a dt to move proportionally to time.
    private move() {
        if (this.goingRight) {
            this.x += this.speed;
        } else {
            this.x -= this.speed;
        }
        this.bounce();
    }

    private bounce() {
        if (this.x < 0) {
            this.x = 0;
            this.setAnimation(WALKING_RIGHT, WALKING_SIZE, true);
        } else if (this.x > SCREEN_WIDTH - RYU_WIDTH) {
            this.x = SCREEN_WIDTH - RYU_WIDTH;
            this.setAnimation(WALKING_LEFT, WALKING_SIZE, false);
        }
    }

    update() {
        this.move();
        this.increaseAnimationFrame(1);
    }
}
