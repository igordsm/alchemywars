///<reference path="list.ts" />
///<reference path="monster.ts" />
///<reference path="ryu.ts" />
///<reference path="board.ts" />

// Pseudo defines for animations.
var SCREEN_WIDTH = 480;
var SCREEN_HEIGHT = 320;

function main() {
    var canvas = <HTMLCanvasElement>document.getElementById("gameCanvas");
    canvas.width = SCREEN_WIDTH;
    canvas.height = SCREEN_HEIGHT;

    var context = canvas.getContext("2d");
    var board = new Board(canvas.width, canvas.height);

    setInterval(function () {board.update(context)}, 100);
}

main();
