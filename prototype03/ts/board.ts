///<reference path="list.ts" />
///<reference path="ryu.ts" />
///<reference path="inputhandler.ts" />

class Board {
    // TODO:
    // 1. Monsters list. -------------------- DONE
    // 2. Button to create new monster. ----- DONE
    // 3. Monsters attacking. ---------------
    // 4. Monsters dying. -------------------
    // 5: Make Point interface for x, y. ---- 
    // 6: Make an image loader. ------------- Done

    private background: HTMLImageElement = null;
    private backgroundReady: bool = false;
    private enemies: List = null;
    private loading: bool = true;
    private inputHandler: InputHandler = null;

    constructor(width: number, height: number) {
        this.background = new Image();
        this.background.src = "images/background.png";
        this.background.width = width;
        this.background.height = height;
        this.background.onload = () => {
            this.backgroundReady = true;
            //console.log("Background image is ready.");
        }
        this.inputHandler = new InputHandler();

        this.enemies = new List();
        for (var i = 0; i < 3; i++) {
            var newEnemy: Ryu = new Ryu(30 + 60 * i, 100);
            this.enemies.push(newEnemy);
        }
    }

    isReady(): bool {
        return this.backgroundReady;
    }

    isLoading(): bool {
        if (this.loading) {
            this.loading = !this.areImagesLoaded();
        }
        return this.loading;
    }

    private areImagesLoaded(): bool {
        if (!this.backgroundReady) {
            return false;
        }
        for (var enemy: ListItem = this.enemies.first(); enemy != undefined;
             enemy = enemy.next) {

            if (!enemy.item.isReadyToDraw()) {
                return false;
            }
        }
        return true;
    }

    // TODO: Implement double buffer.
    // TODO: I don't know which type 'context' should be.
    draw(context) {
        if (this.isLoading()) {
            // Does not draw anything until all images are loaded.
            context.font = "30px Arial";
            context.fillText("Loading", 32, 32);
            return;
        }
        context.drawImage(this.background, 0, 0,
                          this.background.width,this.background.height);

        for (var enemy: ListItem = this.enemies.first(); enemy != undefined;
             enemy = enemy.next) {

            enemy.item.draw(context);
        }
    }

    private handleEvents() {
        if (KEY_STR.SPACE in this.inputHandler.keysPressed) {
            var newEnemy: Ryu = new Ryu(80, 210);
            this.enemies.push(newEnemy);
        }
        this.inputHandler.clear();
    }

    update(context) {
        this.handleEvents();
        for (var enemy: ListItem = this.enemies.first(); enemy != undefined;
             enemy = enemy.next) {

            enemy.item.update();
        }

        // After everything is updated, it is time to draw.
        this.draw(context);
    }
}
