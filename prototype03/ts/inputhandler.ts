class InputHandler {
    public keysDown = {};
    public keysPressed = {};

    constructor() {
        addEventListener("keydown", (event: KeyboardEvent) => {
            this.keysDown[event.keyCode] = true;
        }, false);

        addEventListener("keyup", (event: KeyboardEvent) => {
            delete this.keysDown[event.keyCode]
            this.keysPressed[event.keyCode] = true;
        }, false);
    }

    clear() {
        this.keysDown = {};
        this.keysPressed = {};
    }
}

var KEY_CODE = {
    UP:        38,
    DOWN:      40,
    LEFT:      37,
    RIGHT:     39,
    SPACE:     32,
    ENTER:     13,
    SHIFT:     16,
    BACKSPACE:  8,
    A:         65,
    B:         66,
    C:         67,
    D:         68,
    E:         69,
    F:         70,
    G:         71,
    H:         72,
    I:         73,
    J:         74,
    K:         75,
    L:         76,
    M:         77,
    N:         78,
    O:         79,
    P:         80,
    Q:         81,
    R:         82,
    S:         83,
    T:         84,
    U:         85,
    V:         86,
    W:         87,
    X:         88,
    Y:         89,
    Z:         90,
}

var KEY_STR = {
    UP:        "38",
    DOWN:      "40",
    LEFT:      "37",
    RIGHT:     "39",
    SPACE:     "32",
    ENTER:     "13",
    SHIFT:     "16",
    BACKSPACE:  "8",
    A:         "65",
    B:         "66",
    C:         "67",
    D:         "68",
    E:         "69",
    F:         "70",
    G:         "71",
    H:         "72",
    I:         "73",
    J:         "74",
    K:         "75",
    L:         "76",
    M:         "77",
    N:         "78",
    O:         "79",
    P:         "80",
    Q:         "81",
    R:         "82",
    S:         "83",
    T:         "84",
    U:         "85",
    V:         "86",
    W:         "87",
    X:         "88",
    Y:         "89",
    Z:         "90",
}
