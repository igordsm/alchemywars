interface ListItem {
    item: any;
    next: ListItem;
    previous: ListItem;
}

class List {
    private firstItem: ListItem = undefined;
    private lastItem: ListItem = undefined;
    private listSize: number = 0;

    constructor() {}

    private newListItem(object: any): ListItem {
        return {
            item: object,
            next: undefined,
            previous: undefined
        };
    }

    public first(): ListItem {
        return this.firstItem;
    }

    public last(): ListItem {
        return this.lastItem;
    }

    public size(): number {
        return this.listSize;
    }

    public isEmpty(): bool {
        return (this.listSize == 0);
    }

    // Obs: itemAt(0) is this.firstItem.
    public itemAt(position: number): ListItem {
        if (position < 0 || position >= this.listSize) {
            return undefined;
        }

        var item: ListItem = this.firstItem;
        for (var i = 0; i < position; i++) {
            item = item.next;
        }
        return item;
    }

    public push(object: any): bool {
        if (this.listSize == 0) {
            var newItem: ListItem = this.newListItem(object);
            this.firstItem = newItem;
            this.lastItem = newItem;
            this.listSize++;
            return true;
        }
        return this.addAfter(object, this.lastItem);
    }

    public pop(): ListItem {
        if (this.listSize = 0) {
            return undefined;
        }
        var item = this.lastItem;
        this.remove(this.lastItem);
    }

    public addBefore(object: any, target: ListItem): bool {
        if (target == null || target == undefined) {
            return false;
        }

        var newItem: ListItem = this.newListItem(object);

        newItem.next = target;
        newItem.previous = target.previous;
        target.previous = newItem;
        if (target == this.firstItem) {
            this.firstItem = newItem;
        } else {
            newItem.previous.next = newItem;
        }

        this.listSize++;
        return true;
    }

    public addAfter(object: any, target: ListItem): bool {
        if (target == null || target == undefined) {
            return false;
        }

        var newItem: ListItem = this.newListItem(object);

        newItem.next = target.next;
        newItem.previous = target;
        target.next = newItem;
        if (target == this.lastItem) {
            this.lastItem = newItem;
        } else {
            newItem.next.previous = newItem;
        }

        this.listSize++;
        return true;
    }

    public remove(target: ListItem): bool {
        if (target == null || target == undefined || this.isEmpty()) {
            return false;
        }

        if (this.listSize == 1) {
            this.firstItem = undefined;
            this.lastItem = undefined;
        } else if (target == this.firstItem) {
            target.next.previous = undefined;
            this.firstItem = target.next;
        } else if (target == this.lastItem) {
            target.previous.next = undefined;
            this.lastItem = target.previous;
        } else {
            target.previous.next = target.next;
            target.next.previous = target.previous;
        }
        this.listSize--;
        return true;
    }
}
